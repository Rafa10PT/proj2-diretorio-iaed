/* fs.c
 * Logic for general filesystem operations and operations on the main
 * filesystem tree.
 */
#include <stdio.h>
#include <stdlib.h>

#include "memory.h"
#include "data.h"
#include "fs_avl.h"
#include "fs_hash.h"

/* FS helper function that contains the static variable for the
 * AVL match. Used to get/set its value.
 */
static Fs_AVL_node *
fs_helper_get_set(Fs_AVL_node *value, Fs_helper_status status) {
	/* Variable to store an AVL node that was matched when searching/deleting
	to make it easier to communicate between functions. */
	static Fs_AVL_node *match = NULL;

	if(status == MATCH_SET)
		match = value;

	return match;
}

Fs_AVL_node *fs_helper_set_match(Fs_AVL_node *prev) {
	return fs_helper_get_set(prev, MATCH_SET);
}

Fs_AVL_node *fs_helper_get_match() {
	return fs_helper_get_set(NULL, MATCH_GET);
}

void fs_list_immediate(Fs_node *root) {
	fs_avl_traverse_in_order(root->avl_tree);
}

Fs_node *
fs_get_subitem_insert(Fs_node *parent, char *name, char *value, int depth) {
	parent->avl_tree = fs_avl_insert_r(parent->avl_tree, name);

	{
		/* Get matched node from helper during insertion/collision. */
		Fs_AVL_node *match = fs_helper_get_match();
		/* Create FS node if we did not collide on AVL. */
		if(match->fs_item == NULL)
			match->fs_item = fs_create_item(parent, name, value, depth);

		else if(value != NULL) fs_node_replace_value(match->fs_item, value);

		return match->fs_item;
	}
}

void fs_node_replace_value(Fs_node *node, char *value) {
	if(node->value != NULL) {
		fs_hash_remove(node);
		free(node->value);
	}
	node->value = strdup(value);
}

Fs_node *fs_create_item(Fs_node *parent, char *name, char *value, int depth) {
	Fs_node *ret = (Fs_node *)try_calloc(1, sizeof(Fs_node));

	ret->depth = depth;

	if(name != NULL) {
		ret->name = strdup(name);
		ret->value = value != NULL ? strdup(value) : NULL;
	}

	if(parent != NULL) {
		ret->family.parent = parent;

		if(parent->family.first_child == NULL)
			parent->family.first_child = ret;

		if(parent->family.last_child != NULL)
			parent->family.last_child->family.next_sibling = ret;

		parent->family.last_child = ret;
	}

	return ret;
}

Fs_node *fs_find(Fs_node *node, char *name) {
	Fs_AVL_node *ret = fs_avl_search(node->avl_tree, name);
	if(ret != NULL)
		return ret->fs_item;

	puts(T_NOT_FOUND);
	return NULL;
}

void fs_destroy_item(Fs_node *item) {
	free(item->name);
	if(item->value != NULL) {
		fs_hash_remove(item);
		free(item->value);
	}
	free(item);
}

/* Auxiliary to do the recursion for deleting.
 */
static void fs_delete_all_below_impl(Fs_node *node) {
	if(node == NULL)
		return;

	fs_avl_destroy_all_below(node->avl_tree);
	fs_delete_all_below_impl(node->family.first_child);
	fs_delete_all_below_impl(node->family.next_sibling);
	fs_destroy_item(node);
}

void fs_delete_all_below(Fs_node *node) {
	fs_avl_destroy_all_below(node->avl_tree);
	fs_delete_all_below_impl(node->family.first_child);
	fs_destroy_item(node);
}

/* Auxiliary to adjust the AVL of a parent node when deleting one of
 * its children.
 */
static Fs_AVL_node *adjust_avl(Fs_node *parent_node, char *element_name) {
	return fs_avl_delete_r(parent_node->avl_tree, element_name);
}

/* Auxiliary to adjust the family a node belongs to before it is removed.
 */
static void adjust_family(Fs_node *ptr) {
	Fs_node *parent = ptr->family.parent;
	Fs_node *it = parent->family.first_child;

	/* If the removed element is the first child. */
	if(ptr == it)
		parent->family.first_child = it->family.next_sibling;

	{
		Fs_node *p = NULL;
		for(; it != ptr; it = it->family.next_sibling)
			p = it;

		/* If the removed element is the last child. */
		if(ptr == parent->family.last_child)
			parent->family.last_child = p;

		if(p != NULL)
			/* Fix sibling list. */
			p->family.next_sibling = it->family.next_sibling;
	}
}

void
fs_delete_all_below_adjust_structs(Fs_node *parent_node, char *element_name) {
	/* Adjusting AVL. */
	parent_node->avl_tree = adjust_avl(parent_node, element_name);

	{
		Fs_AVL_node *ptr = fs_helper_get_match();

		if(ptr == NULL) {
			puts(T_NOT_FOUND);
			return;
		}

		adjust_family(ptr->fs_item);

		/* Deleting all below. */
		fs_delete_all_below(ptr->fs_item);
		/* Destroy the AVL node that was from the parent AVL tree. */
		fs_avl_destroy_item(ptr);
	}
}

void fs_root_delete_all_below(Fs_node *parent_node) {
	fs_delete_all_below_impl(parent_node->family.first_child);
	fs_avl_destroy_all_below(parent_node->avl_tree);

	/* We don't free the root, but some elements have to become NULL again. */
	parent_node->family.first_child = parent_node->family.last_child = NULL;
	parent_node->avl_tree = NULL;
}
