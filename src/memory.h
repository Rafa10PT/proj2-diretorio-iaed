/* memory.h
 * Information of memory functions.
 * Includes their function prototypes.
 * Functions to help managing the memory of the program and wrappers for
 * libc allocation functions to output an error if allocation is impossible.
 */
#ifndef MEMORY_H
#define MEMORY_H

#include "data.h"
#include "fs.h"

/* The ISO C23 strdup function is allowed in this project.
 */
char *strdup(const char *str);

/* Creates the root node of the filesystem tree and returns it.
 */
Fs_node *create_fs_root();

/* Handles the memory objects. If NULL is passed, they are created and
 * saved. Else, they are completely freed with their subobjects.
 */
void handle_mem(Fs_node *obj);

/* Frees all allocated memory.
 */
void free_everything();

/* Tries to malloc a block of a certain size. If malloc fails,
 * the memory is all freed and the execution is stopped along with a
 * "No memory" error.
 */
void *try_malloc(size_t size);

/* Tries to calloc a block of a certain nitems*size. If calloc fails,
 * the memory is all freed and the execution is stopped along with a
 * "No memory" error.
 */
void *try_calloc(size_t nitems, size_t size);

/* Tries to realloc a block pointed by ptr to a certain size. If realloc fails,
 * the memory is all freed and the execution is stopped along with a
 * "No memory" error.
 */
void *try_realloc(void *ptr, size_t size);

#endif /* MEMORY_H */
