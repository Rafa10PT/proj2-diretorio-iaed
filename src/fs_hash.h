/* fs_hash.h
 * Information for the hash table used for storing nodes based on their value.
 * Includes function prototypes, data structures, constants and enumerations.
 */
#ifndef FS_HASH_H
#define FS_HASH_H

#include "fs.h"

/* Hash constants. */
#define HASH_TABLE_AMOUNT 0x40000
#define HASH_MODULO (unsigned long)(1e9 + 7)
#define HASH_MULTIPLIER 257

typedef enum {
	HASH_INSERT = 0,
	HASH_REMOVE = 1,
	HASH_GET = 2,
	HASH_FREE = 3
} Fs_hash_access;

typedef struct fs_hash_node {
	struct fs_hash_node *next;
	Fs_node *item;
} Fs_hash_node;

/* Creates a hash table for the values of the elements of a node if
 * we do not have one yet. Will handle inserting, removing or getting
 * elements from the hash table.
 */
Fs_node *fs_hash_handle_table(Fs_node *element, Fs_hash_access access);

/* Inserts a node in the hash table based on its value in the order of the
 * elements in the FS node tree.
 */
void fs_hash_insert(Fs_node *node);

/* Removes a node from the hash table based on its value in the order of the
 * elements in the FS node tree.
 */
void fs_hash_remove(Fs_node *node);

/* Gets a value from the hash table (the first one found that matches the
 * passed string). The first node with string s that is first from a
 * a filesystem tree perspective (the first element of its path is older than
 * the first element of the path of any other node with the same string as
 * value).
 */
Fs_node *fs_hash_get(char *s);

/* Frees the whole hash table and its hash nodes.
 */
void fs_hash_free();

/* Computes a hash value for an ASCII string using a
 * polynomial rolling algorithm.
 */
unsigned int fs_hash_compute(const char *s);

#endif /* FS_HASH_H */