/* fs_avl.c
 * Logic for functions used on AVL trees of the filesystem nodes.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "memory.h"
#include "data.h"
#include "fs_avl.h"

Fs_AVL_node *fs_avl_create_item() {
	Fs_AVL_node *ret = (Fs_AVL_node *)try_calloc(1, sizeof(Fs_AVL_node));

	ret->height = 1;

	return ret;
}

void fs_avl_destroy_item(Fs_AVL_node *node) {
	free(node);
}

void fs_avl_destroy_all_below(Fs_AVL_node *node) {
	if(node == NULL)
		return;

	fs_avl_destroy_all_below(node->left);

	fs_avl_destroy_all_below(node->right);

	fs_avl_destroy_item(node);
}

int fs_avl_height(Fs_AVL_node *node) {
	return node == NULL ? 0 : node->height;
}

Fs_AVL_node *fs_avl_max(Fs_AVL_node *node) {
	while(node != NULL && node->right != NULL)
		node = node->right;

	return node;
}

Fs_AVL_node *fs_avl_min(Fs_AVL_node *node) {
	while(node != NULL && node->left != NULL)
		node = node->left;

	return node;
}

/* This function will recalculate the height of the 2 parsed nodes.
 */
static void height_update(Fs_AVL_node *node, Fs_AVL_node *x) {
	int left = fs_avl_height(node->left);
	int right = fs_avl_height(node->right);
	node->height = left > right ? left + 1 : right + 1;

	left = fs_avl_height(x->left);
	right = fs_avl_height(x->right);
	x->height = left > right ? left + 1 : right + 1;
}

Fs_AVL_node *fs_avl_rot_l(Fs_AVL_node *node) {
	Fs_AVL_node *x = node->right;
	node->right = x->left;
	x->left = node;

	height_update(node, x);

	return x;
}

Fs_AVL_node *fs_avl_rot_r(Fs_AVL_node *node) {
	Fs_AVL_node *x = node->left;
	node->left = x->right;
	x->right = node;

	height_update(node, x);

	return x;
}

Fs_AVL_node *fs_avl_rot_lr(Fs_AVL_node *node) {
	if(node == NULL) return node;
	node->left = fs_avl_rot_l(node->left);
	return fs_avl_rot_r(node);
}

Fs_AVL_node *fs_avl_rot_rl(Fs_AVL_node *node) {
	if(node == NULL) return node;
	node->right = fs_avl_rot_r(node->right);
	return fs_avl_rot_l(node);
}

int fs_avl_balance(Fs_AVL_node *node) {
	if(node == NULL) return 0;
	
	return fs_avl_height(node->left)
	- fs_avl_height(node->right);
}

Fs_AVL_node *fs_avl_avlbalance(Fs_AVL_node *node) {
	if(node != NULL) {
		int balance_factor = fs_avl_balance(node);

		/* Chose according to balance factor values. */
		if(balance_factor > 1)
			node = fs_avl_balance(node->left) >= 0 ?
			fs_avl_rot_r(node) : fs_avl_rot_lr(node);

		else if(balance_factor < -1)
			node = fs_avl_balance(node->right) <= 0 ?
			fs_avl_rot_l(node) : fs_avl_rot_rl(node);

		else {
			int left = fs_avl_height(node->left);
			int right = fs_avl_height(node->right);

			node->height = left > right ? left + 1 : right + 1;
		}
	}

	return node;
}

void fs_avl_traverse_in_order(Fs_AVL_node *node) {
	if(node == NULL)
		return;

	fs_avl_traverse_in_order(node->left);
	puts(node->fs_item->name);
	fs_avl_traverse_in_order(node->right);
}

Fs_AVL_node *fs_avl_insert_r(Fs_AVL_node *node, char *name) {
	if (node == NULL) {
		/* No collision, hence we make a new node. */
		Fs_AVL_node *ret = fs_avl_create_item();

		/* Set helper match as the inserted node. */
		fs_helper_set_match(ret);

		return ret;
	}

	{
		/* Using alphabetical order for the AVL. */
		const int cmp = strcmp(name, node->fs_item->name);

		/* Smaller values go to left. */
		if(cmp < 0)
			node->left = fs_avl_insert_r(node->left, name);

		/* Bigger values go to right. */
		else if(cmp > 0)
			node->right = fs_avl_insert_r(node->right, name);

		/* Equality means we have a collision. */
		else {
			/* Set helper match as the colided node and return. */
			fs_helper_set_match(node);
			return node;
		}

		/* Rebalance the AVL BST. */
		node = fs_avl_avlbalance(node);

		return node;
	}
}

Fs_AVL_node *fs_avl_search(Fs_AVL_node *node, char *name) {
	if(node != NULL) {
		int cmp = strcmp(name, node->fs_item->name);
		/* Value found, we can return. */
		if(cmp == 0)
			return node;

		if(cmp < 0)
			return fs_avl_search(node->left, name);

		return fs_avl_search(node->right, name);
	}

	return NULL;
}

/* This function will swap the fs_items between 2 nodes.
 */
static void swap(Fs_AVL_node *a, Fs_AVL_node *b) {
	Fs_node *prev = a->fs_item;
	a->fs_item = b->fs_item;
	b->fs_item = prev;
}

Fs_AVL_node *fs_avl_delete_r(Fs_AVL_node *node, char *name) {
	if(node != NULL) {
		int cmp = strcmp(name, node->fs_item->name);
		if(cmp < 0)
			node->left = fs_avl_delete_r(node->left, name);

		else if(cmp > 0)
			node->right = fs_avl_delete_r(node->right, name);

		else {
			if(node->left != NULL && node->right != NULL) {
				Fs_AVL_node *aux = fs_avl_max(node->left);
				/* Swap items (so that we have correct heights, pointers, etc.)
				*/
				swap(aux, node);
				node->left = fs_avl_delete_r(node->left, aux->fs_item->name);
			}

			else {
				Fs_AVL_node *aux = node;
				if(node->left == NULL && node->right == NULL)
					node = NULL;

				else node = node->left == NULL ? node->right : node->left;

				/* Send the matched excluded node to the match with
				FS helper. */
				fs_helper_set_match(aux);
			}
		}

		/* Rebalance the AVL BST. */
		node = fs_avl_avlbalance(node);
	}

	return node;
}
