/* fs_avl.h
 * Information for AVL trees of the filesystem nodes.
 * Includes function prototypes and data structures.
 */
#ifndef FS_AVL_H
#define FS_AVL_H

#include "fs.h"

/* AVL tree for the contents inside an element
 * to organize them alphabetically (ASCII order).
 */
struct fs_avl_node {
	/* We have a pointer to a FS_node as the item, but we organize
	based on fs_item->name. */
	Fs_node *fs_item;
	struct fs_avl_node *left;
	struct fs_avl_node *right;
	int height;
};

/* This function creates an AVL file node and returns it.
 */
Fs_AVL_node *fs_avl_create_item();

/* This function destroys an AVL file node.
 */
void fs_avl_destroy_item(Fs_AVL_node *node);

/* This function destroys an AVL file node along with its nodes below.
 */
void fs_avl_destroy_all_below(Fs_AVL_node *node);

/* This function returns the height of an AVL node or 0 if NULL is passed.
 */
int fs_avl_height(Fs_AVL_node *node);

/* This function returns the maximum value in an AVL tree.
 */
Fs_AVL_node *fs_avl_max(Fs_AVL_node *node);

/* This function returns the minimum value in an AVL tree.
 */
Fs_AVL_node *fs_avl_min(Fs_AVL_node *node);

/* This function does an AVL left rotation on an AVL node
 * and returns the resulting node.
 */
Fs_AVL_node *fs_avl_rot_l(Fs_AVL_node *node);

/* This function does an AVL right rotation on an AVL node
 * and returns the resulting node.
 */
Fs_AVL_node *fs_avl_rot_r(Fs_AVL_node *node);

/* This function does an AVL left-right rotation on an AVL node
 * and returns the resulting node.
 */
Fs_AVL_node *fs_avl_rot_lr(Fs_AVL_node *node);

/* This function does an AVL right-left rotation on an AVL node
 * and returns the resulting node.
 */
Fs_AVL_node *fs_avl_rot_rl(Fs_AVL_node *node);

/* This function returns the balance factor of an AVL node.
 */
int fs_avl_balance(Fs_AVL_node *node);

/* This function performs balance on an AVL node
 * and returns the resulting node.
 */
Fs_AVL_node *fs_avl_avlbalance(Fs_AVL_node *node);

/* This function will traverse the AVL tree under a certain node in order, 
 * logging their names.
 */
void fs_avl_traverse_in_order(Fs_AVL_node *node);

/* This function tries to perform insertion of a node under the one from
 * the first arg with a certain name and a value. If there is already a node
 * with the same name under the current path, we have a collision and the value
 * is replaced in case the third argument is not a NULL while keeping the
 * already existing node. The inserted/colided node is sent to the match
 * with fs_helper_set_match. The resulting node of the AVL insertion
 * (after balances) is returned.
 */
Fs_AVL_node *fs_avl_insert_r(Fs_AVL_node *node, char *name);

/* This function looks for a node with a certain name and returns it
 * if found. Else, NULL is returned.
 */
Fs_AVL_node *fs_avl_search(Fs_AVL_node *node, char *name);

/* This function will delete a node with a certain name from the AVL tree.
 * The resulting node of the AVL deletion (after balances) is returned. The
 * node that was excluded is sent to the match with fs_helper_set_match.
 */
Fs_AVL_node *fs_avl_delete_r(Fs_AVL_node *node, char *name);

#endif /* FS_AVL_H */
