/* commands.c
 * Logic for commands functions.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "memory.h"
#include "data.h"
#include "commands.h"

/* This predicate returns true if the parsed char is a space (spacebar or tab)
 * and false otherwise.
 */
static bool isspace_char(char c) {
	return c == ' ' || c == '\t';
}


void list_commands() {
	puts(T_COMMAND_INFO);
}

void set_path_value(Fs_node *fs_tree) {
	char path[INPUT_MAX], value[INPUT_MAX];
	/* Read path. */
	scanf("%s", path);

	/* Read value. */
	while(isspace_char(value[0] = getchar()));

	fgets(value + 1, INPUT_MAX - 1, stdin);
	value[strlen(value) - 1] = '\0';

	{
		char *prev = strtok(path, "/"), *token;
		int depth = 1;
		while((token = strtok(NULL, "/")) != NULL) {
			/* Passing NULL as value means we won't set any value. */
			/* Since we are just going through path here, we don't set. */
			fs_tree = fs_get_subitem_insert(fs_tree, prev, NULL, depth++);
			prev = token;
		}

		/* This is the insertion where we want to set. */
		fs_hash_insert(fs_get_subitem_insert(fs_tree, prev, value, depth++));
	}
}

/* Auxiliary recursive function for print_paths.
 */
static
void print_paths_impl(Fs_node *node, String_node *path, String_node *final) {
	if(node != NULL) {
		/* Setup a subnode. */
		String_node new_path = {NULL};

		new_path.value = node->name;

		final->next = &new_path;

		if(node->value != NULL) {
			String_node *it;
			/* Has a value, hence we print the full path and value. */
			for(it = path->next; it != NULL; it = it->next)
				printf("/%s", it->value);
			printf(" %s\n", node->value);
		}

		/* Recursion for children. */
		print_paths_impl(node->family.first_child, path, &new_path);

		/* Recursion for siblings. */
		print_paths_impl(node->family.next_sibling, path, final);
	}
}

void print_paths(Fs_node *node) {
	String_node master = {NULL};

	print_paths_impl(node->family.first_child, &master, &master);
}

/* Auxiliary recursive function for find_path.
 */
static Fs_node *find_path_impl(Fs_node *root, char *str) {
	if(str != NULL) {
		Fs_node *search = fs_find(root, str);

		if(search != NULL)
			return find_path_impl(search, strtok(NULL, "/"));

		return NULL;
	}

	return root;
}

void find_path(Fs_node *root) {
	char path[INPUT_MAX];

	while(isspace_char(path[0] = getchar()));

	fgets(path + 1, INPUT_MAX - 1, stdin);
	path[strlen(path) - 1] = '\0';

	{
		/* Start recursion where we'll keep breaking in tokens. */
		Fs_node *node = find_path_impl(root, strtok(path, "/"));

		if(node == NULL)
			return;

		if(node->value == NULL)
			puts(T_NO_DATA);

		else
			puts(node->value);
	}
}

void list_paths(Fs_node *root) {
	char path[INPUT_MAX];

	while(isspace_char(path[0] = getchar()));

	if(path[0] != '\n') {
		fgets(path + 1, INPUT_MAX - 1, stdin);
		path[strlen(path) - 1] = '\0';
	}

	/* In case we don't detect anything. */
	else path[0] = '\0';

	{
		char *tok = strtok(path, "/");
		Fs_node *node = root;
		if(tok != NULL)
			node = find_path_impl(root, tok);

		if(node == NULL)
			return;

		else
			fs_list_immediate(node);
	}
}

/* Recursive function to display the full path of the element of a node.
 */
static void display_full_path(Fs_node *node, String_node *str_node) {
	if(node->family.parent == NULL) {
		/* We reached root. */
		for(str_node = str_node->next; str_node != NULL;
		str_node = str_node->next)
			printf("/%s", str_node->value);
		puts("");
	}

	else {
		/* Add another node in. */
		String_node new_node;
		new_node.value = node->family.parent->name;
		new_node.next = str_node;
		display_full_path(node->family.parent, &new_node);
	}
}

void search_value() {
	char value[INPUT_MAX];

	while(isspace_char(value[0] = getchar()));
	fgets(value + 1, INPUT_MAX - 1, stdin);
	value[strlen(value) - 1] = '\0';

	{
		/* Get value from hash table. */
		Fs_node *node = fs_hash_get(value);
		if(node != NULL) {
			String_node str_node;
			str_node.next = NULL;
			str_node.value = node->name;
			display_full_path(node, &str_node);
		}

		/* If not in hash table, it doesn't exist. */
		else puts(T_NOT_FOUND);
	}
}

void delete_paths(Fs_node *root) {
	char path[INPUT_MAX];

	while(isspace_char(path[0] = getchar()));

	if(path[0] != '\n') {
		fgets(path + 1, INPUT_MAX - 1, stdin);
		path[strlen(path) - 1] = '\0';
	}

	/* In case we don't detect anything. */
	else path[0] = '\0';

	{
		char *prev = strtok(path, "/"), *token;
		Fs_node *node = root;
		if(prev != NULL) {
			while((token = strtok(NULL, "/")) != NULL) {
				if((node = fs_find(node, prev)) == NULL)
					return;
				prev = token;
			}

			fs_helper_set_match(NULL);
			fs_delete_all_below_adjust_structs(node, prev);
		}

		else fs_root_delete_all_below(root);
	}
}
