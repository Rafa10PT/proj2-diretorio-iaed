/* fs_hash.c
 * Logic for the hash table used for storing nodes based on their value.
 */
#include <stdlib.h>
#include <string.h>

#include "memory.h"
#include "data.h"
#include "fs_hash.h"

/* Go above until we reach the desired depth (parsed value).
 */
static Fs_node *adjust_depth_impl(Fs_node *node, int depth) {
	Fs_node *parent;
	for(parent = node->family.parent;
	parent->depth != depth; parent = parent->family.parent);
	return parent;
}

/* Function that adjusts the depth of the deepest node between node or against.
 * Adjusting so that it is on the same level when we look for latest common
 * ancestor. We return true if node is deeper than against and false otherwise.
 */
static bool adjust_depths(Fs_node **node, Fs_node **against) {
	if((*node)->depth > (*against)->depth) {
		*node = adjust_depth_impl(*node, (*against)->depth);
		return true;
	}

	*against = adjust_depth_impl(*against, (*node)->depth);
	return false;
}

/* Auxiliary function that checks if node is newer than against
 * from a filesystem tree perspective (we get the lowest common ancestor
 * and check if the newer child element of node is newer than the one from 
 * the path of against).
 */
static bool is_newer(Fs_node *node, Fs_node *against) {
	Fs_node *prev_node = NULL, *prev_against = NULL;

	if(node->depth != against->depth) {
		bool ret = adjust_depths(&node, &against);

		/* If they are in the same parent line. */
		if(node == against)
			/* True if node is deeper and false otherwise.*/
			return ret; 
	}

	for(; node != against;
	node = node->family.parent, against = against->family.parent) {
		prev_node = node;
		prev_against = against;
	}

	{
		Fs_node *it;
		for(it = prev_node->family.parent->family.first_child;
		it != NULL; it = it->family.next_sibling) {
			if(it == prev_node)
				return false;
			else if(it == prev_against)
				return true;
		}
	}

	return false;
}

/* Auxiliary function to insert an element.
 */
static Fs_hash_node *fs_hash_insert_impl(Fs_hash_node *list, Fs_node *element) {
	Fs_hash_node *node = (Fs_hash_node *)try_malloc(sizeof(Fs_hash_node));

	node->next = NULL;
	node->item = element;

	if(list == NULL)
		/* First element. */
		return node;

	else {
		/* More recent is inserted on bottom. */
		Fs_hash_node *it = list, *prev = NULL;
		/* Checks if the element is newer in the FS tree. */
		for(; it != NULL && is_newer(element, it->item); it = it->next)
			prev = it;

		node->next = it;
		if(prev != NULL)
			prev->next = node;

		/* First element. */
		else return node;
	}

	return list;
}

/* Auxiliary function to remove an element.
 */
static Fs_hash_node *fs_hash_remove_impl(Fs_hash_node *list, Fs_node *element) {
	Fs_hash_node *prev = NULL;
	Fs_hash_node *it = list;

	for(; it->item != element; it = it->next) {
		prev = it;
	}

	if(prev != NULL) {
		prev->next = it->next;
		free(it);
	}

	else {
		/* If we are removing from the very top. */
		list = list->next;
		free(it);
	}

	return list;
}

/* Auxiliary function to get the node.
 */
static Fs_node *fs_hash_get_impl(Fs_hash_node *list, char *s) {
	Fs_hash_node *it;
	/* Gets first value that matches the string
	(we have to strcmp in case we share hashes). */
	for(it = list; it != NULL && strcmp(it->item->value, s) != 0;
	it = it->next);

	if(it == NULL) return NULL;

	return it->item;
}

/* The hash table is a static variable of this function.
 * We use this function to operate the table.
 */
Fs_node *fs_hash_handle_table(Fs_node *element, Fs_hash_access access) {
	/* Hash table to store nodes based on their value. */
	static Fs_hash_node **table = NULL;

	if(table == NULL)
		table = try_calloc(HASH_TABLE_AMOUNT, sizeof(String_node *));

	if(access == HASH_FREE) {
		if(table != NULL)
			free(table);

		return NULL;
	}

	{
		const int id = fs_hash_compute(element->value);
		Fs_hash_node *list = table[id];

		if(access == HASH_INSERT)
			table[id] = fs_hash_insert_impl(list, element);

		else if(access == HASH_REMOVE)
			table[id] = fs_hash_remove_impl(list, element);

		else /*if(access == HASH_GET)*/
			return fs_hash_get_impl(list, element->value);
	}

	return NULL;
}

void fs_hash_insert(Fs_node *node) {
	fs_hash_handle_table(node, HASH_INSERT);
}

void fs_hash_remove(Fs_node *node) {
	fs_hash_handle_table(node, HASH_REMOVE);
}

Fs_node *fs_hash_get(char *s) {
	Fs_node elm;
	elm.value = s;
	return fs_hash_handle_table(&elm, HASH_GET);
}

void fs_hash_free() {
	fs_hash_handle_table(NULL, HASH_FREE);
}

unsigned int fs_hash_compute(const char *s) {
	/* HASH_MULTIPLIER to the power of 0 is 1. */
	unsigned long pow = 1;
	unsigned long ret = 0;

	while(*s) {
		/* We increment the hash by the value that results from
		multiplying a character to a power of HASH_MULTIPLIER whose exponent
		increments by 1 each iteration and applying a modulo with HASH_MODULO.*/
		ret = ret + (*s++ * pow) % HASH_MODULO;
		/* We set pow to HASH_MULTIPLIER to the power of the next
		exponent by multiplying with itself. */
		pow = (pow * HASH_MULTIPLIER) % HASH_MODULO;
	}

	/* Return the result modulo the table amount so that it is still
	in range. */
	return ret % HASH_TABLE_AMOUNT;
}
