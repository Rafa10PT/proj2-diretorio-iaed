/* data.h
 * Defines some important constant data, includes important headers and defines
 * types. Important contents that are used all over the project.
 */
#ifndef DATA_H
#define DATA_H

/* Constants. */
#define INPUT_MAX 65537

/* Text. */
#define T_NO_MEMORY "No memory"
#define T_COMMAND_INFO  "help: Imprime os comandos disponíveis.\n"\
						"quit: Termina o programa.\n"\
						"set: Adiciona ou modifica o valor a armazenar.\n"\
						"print: Imprime todos os caminhos e valores.\n"\
						"find: Imprime o valor armazenado.\n"\
						"list: Lista todos os componentes "\
						"imediatos de um sub-caminho.\n"\
						"search: Procura o caminho dado um valor.\n"\
						"delete: Apaga um caminho e todos os subcaminhos."
#define T_NOT_FOUND "not found"
#define T_NO_DATA "no data"

/* Types. */
typedef enum {false = 0, true = 1} bool;

#endif /* DATA_H */
