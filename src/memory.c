/* memory.c
 * Logic for memory functions.
 * Functions to help managing the memory of the program and wrappers for
 * libc allocation functions to output an error if allocation is impossible.
 */
#include <stdio.h>
#include <stdlib.h>

#include "memory.h"
#include "fs_hash.h"

Fs_node *create_fs_root() {
	return fs_create_item(NULL, "", NULL, 0);
}

void handle_mem(Fs_node *obj) {
	/* Variable to save the root of the filsystem tree so that it knows where
	to start freeing the memory when we are exiting execution. */
	static Fs_node *root = NULL;

	/* Setting the value if not NULL. */
	if(obj != NULL) {
		root = obj;
		return;
	}

	/* Else, we delete everything. */
	fs_delete_all_below(root);
}

void free_everything() {
	/* Passing NULL means we will free all the filesystem nodes and AVLs. */
	handle_mem(NULL);
	/* Free hash table as well. */
	fs_hash_free();
}

/* Auxiliary function that displays the "No memory" error,
 * frees everything and stops the execution.
 */
static void terminate_exec() {
	/* Error message. */
	puts(T_NO_MEMORY);

	/* Free allocated memory. */
	free_everything();

	/* Exit. */
	exit(0);
}

void *try_malloc(size_t size) {
	void *ret = malloc(size);

	/* No more memory. */
	if(ret == NULL)
		terminate_exec();

	return ret;
}

void *try_calloc(size_t nitems, size_t size) {
	void *ret = calloc(nitems, size);

	/* No more memory. */
	if(ret == NULL)
		terminate_exec();

	return ret;
}

void *try_realloc(void *ptr, size_t size) {
	void *ret = realloc(ptr, size);

	/* No more memory. */
	if(ret == NULL)
		terminate_exec();

	return ret;
}
