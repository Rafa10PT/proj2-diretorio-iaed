/* fs.c
 * Information of general filesystem operations and operations on the main
 * filesystem tree.
 * Includes function prototypes, data structures and enumerations.
 */
#ifndef FS_H
#define FS_H

typedef struct fs_avl_node Fs_AVL_node;

/* Family of an element.
 */
typedef struct fs_family_node {
	/* Points to a child and can be used to iterate through
	children until NULL is found. */
	struct fs_node *first_child;
	/* Used to make it easier to insert a child. */
	struct fs_node *last_child;
	/* Points to the parent and can be used to iterate through the ancestors
	until NULL is found. */
	struct fs_node *parent;
	/* Points to the next sibling and can be used to iterate through
	siblings until NULL is found. */
	struct fs_node *next_sibling;
} Fs_family_node;

/* Node of the filesystem tree.
 * Each node represents an element and each element can have
 * a name, an associated value and elements inside of it.
 */
typedef struct fs_node {
	char *name;
	char *value;
	Fs_family_node family;
	/* AVL tree for children of a node to help ASCII order searching. */
	Fs_AVL_node *avl_tree;
	/* Depth of a node in the whole filesystem. */
	int depth;
} Fs_node;

/* String nodes to accumulate paths.
 */
typedef struct string_node {
	struct string_node *next;
	char *value;
} String_node;

/* Status for the get/set function fs_helper_get_set.
 * The FS helper functions get or set the match static variable
 * for data matched on AVL tree operations since we can't
 * use globals. The AVL match is used to communicate between functions a matched
 * node found in a FS AVL tree.
 */
typedef enum {
	MATCH_SET = 0,
	MATCH_GET = 1
} Fs_helper_status;

/* FS helper wrapper for setting the AVL match to a value.
 * The same value is returned.
 */
Fs_AVL_node *fs_helper_set_match(Fs_AVL_node *match);

/* FS helper wrapper for getting the AVL match value.
 */
Fs_AVL_node *fs_helper_get_match();

/* This function will list alphabetically all the sub-elements of
 * a certain element node.
 */
void fs_list_immediate(Fs_node *root);

/* Function that inserts an item in the filesystem under a parent node.
 * If it already exists, its value will be replaced without replacing the node.
 * The address of the found/inserted node is returned.
 */
Fs_node *
fs_get_subitem_insert(Fs_node *parent, char *name, char *value, int depth);

/* This function will replace the value of a certain node with a newer one.
 * The previous one is freed and completely replaced if different than NULL.
 */
void fs_node_replace_value(Fs_node *node, char *value);

/* Function that creates a node for the FS and returns it.
 */
Fs_node *fs_create_item(Fs_node *parent, char *name, char *value, int depth);

/* Tries to find a node with a certain name under another node and returns it
 * if found. Else, NULL is returned and a "not found" message is printed.
 */
Fs_node *fs_find(Fs_node *node, char *name);

/* Function that destroys a parsed function node.
 */
void fs_destroy_item(Fs_node *item);

/* This function will delete an element along with their subelements
 * from the filesystem tree.
 */
void fs_delete_all_below(Fs_node *node);

/* This function will delete an element along with their subelements
 * from the filesystem tree and adjust the AVL tree and linked list accordingly.
 * Parent pointer and element name are passed. If the element is not found,
 * nothing is done and a "not found" message is displayed.
 */
void
fs_delete_all_below_adjust_structs(Fs_node *parent_node, char *element_name);

/* This function will delete the subelements of a node.
 * conidering a tree root perspective. Hence, the AVL and linked list
 * NULLed as well and the parent node remains as root ready to add new
 * files all over again!
 */
void fs_root_delete_all_below(Fs_node *parent_node);

#endif /* FS_H */
