/* commands.h
 * Prototypes for commands functions.
 */
#ifndef COMMANDS_H
#define COMMANDS_H

#include "fs.h"
#include "fs_hash.h"

/* The command input between parenthesis has already been evaluated in main. */

/* This function will output the list of available commands.
 * Input: (help)
 * Output: <command>: <description>
 */
void list_commands();

/* This function will set the value of a file in the filesystem.
 * The element path is separated with '/'.
 * Input: (set) </element/path> <value>
 */
void set_path_value(Fs_node *fs_tree);

/* This function will output all paths and values (one path and value)
 * per line, according to the order of creation of the elements. Only the
 * paths which contain an associated value will be outputed.
 * The output paths start and are separated with '/'.
 * Input: (print).
 * Output: </element/path> <value>.
 */
void print_paths(Fs_node *root);

/* This function will output the value associated with a certain path.
 * The element path is separated with '/'.
 * Input: (find) </element/path>
 * Output: <value>
 * Errors:
 * "not found": if the path does not exist.
 * "no data": if the path does not hold an associated value.
 */
void find_path(Fs_node *root);

/* Lists all the immediate elements of a path alphabetically (ASCII order),
 * just the contents of the directory not including the subcontents of them.
 * Input: (list) <path>
 * Output (per line): <name_of_element>
 * Errors:
 * "not found" - if the path does not exist.
 */
void list_paths(Fs_node *root);

/* Searches a path given a value. The first found path that
 * contains exactly the parsed value is output. Each component is searched
 * by creation order.
 * The output paths start and are separated with '/'.
 * Errors:
 * "not found" - if the path does not exist.
 */
void search_value();

/* This will delete the element on the passed path
 * and every element included in a subpath of this path.
 * The element path is separated with '/'.
 * Input: (delete) </element/path>
 * Errors:
 * "not found" - if the path does not exist.
 */
void delete_paths(Fs_node *root);

#endif /* COMMANDS_H */
