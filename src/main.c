/* main.c
 * Starting point of the program.
 */
#include <stdio.h>
#include <string.h>

#include "memory.h"
#include "data.h"
#include "commands.h"

int main() {
	/* Input from stdin. */
	char input[INPUT_MAX];

	/* Boolean to allow the program to run. */
	bool running = true;

	/* The filesystem tree starts on root '/'. */
	Fs_node *fs_tree = create_fs_root();

	/* Parse the root node to allow freeing later. */
	handle_mem(fs_tree);

	/* Main application loop. */
	do {
		scanf("%s", input);

		/* Check the commands that can match the input. */
		if(!strcmp(input, "help"))
			list_commands();

		else if(!strcmp(input, "quit"))
			/* The input "quit" is a command to stop execution. */
			running = false;

		else if(!strcmp(input, "set"))
			set_path_value(fs_tree);

		else if(!strcmp(input, "print"))
			print_paths(fs_tree);

		else if(!strcmp(input, "find"))
			find_path(fs_tree);

		else if(!strcmp(input, "list"))
			list_paths(fs_tree);

		else if(!strcmp(input, "search"))
			search_value(fs_tree);

		else if(!strcmp(input, "delete"))
			delete_paths(fs_tree);
	} while(running);

	/* Free allocated memory. */
	free_everything();

	/* Exit. */
	return 0;
}
