# Based on libnx Makefile.
TOPDIR ?= $(CURDIR)

TARGET	 = $(notdir $(CURDIR))
BUILD	 = build
SOURCES  = src

#-----Compilers-----

CC	= gcc

CXX = g++

#------------------

#-----Get Flags---------

CFLAGS = -ansi -Wall -Wextra -g -pedantic

DEPENDENCY_FLAGS = -MMD -MP -MF $(DEPSDIR)/$*.d

#----------------------

ifneq ($(BUILD),$(notdir $(CURDIR)))

# Phony $(BUILD) so that the fact that it still executes even if it exists.
.PHONY: $(BUILD) all clean dist-mooshak

export TOPDIR	:=	$(CURDIR)

export OUTPUT	:=	$(CURDIR)/$(TARGET)

export VPATH	:=	$(foreach dir,$(SOURCES),$(CURDIR)/$(dir))

export DEPSDIR	:=	$(CURDIR)/$(BUILD)

CFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.c)))
CPPFILES	:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.cpp)))
SFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.s)))
BINFILES	:=	$(foreach dir,$(DATA),$(notdir $(wildcard $(dir)/*.*)))
#-------------------------------------------------------------------------------
# use CXX for linking C++ projects, CC for standard C
#-------------------------------------------------------------------------------
ifeq ($(strip $(CPPFILES)),)
#-------------------------------------------------------------------------------
	export LD	:=	$(CC)
#-------------------------------------------------------------------------------
else
#-------------------------------------------------------------------------------
	export LD	:=	$(CXX)
#-------------------------------------------------------------------------------
endif
#-------------------------------------------------------------------------------

export OFILES_BIN	:=	$(addsuffix .o,$(BINFILES))
export OFILES_SRC	:=	$(CPPFILES:.cpp=.o) $(CFILES:.c=.o) $(SFILES:.s=.o)
export OFILES 	:=	$(OFILES_BIN) $(OFILES_SRC)
export HFILES_BIN	:=	$(addsuffix .h,$(subst .,_,$(BINFILES)))

all: $(BUILD)
dist-mooshak: $(BUILD) $(OUTPUT).zip

$(BUILD):
	@[ -d $@ ] || mkdir -p $@
	@$(MAKE) --no-print-directory -C $(BUILD) -f $(CURDIR)/Makefile

$(OUTPUT).zip	:	$(OUTPUT).exe
	zip -jr $@ $(SOURCES)

clean:
	@echo clean ...
	@rm -rf $(BUILD) $(TARGET).exe
	@rm -rf $(BUILD) $(TARGET).zip

#-------------------------------------------------------------------------------
else
.PHONY: all

DEPENDS	:=	$(OFILES:.o=.d)

#-------------------------------------------------------------------------------
# main targets
#-------------------------------------------------------------------------------
all	:	$(OUTPUT).exe

$(OUTPUT).exe	:	$(OFILES)
	$(LD) $(OFILES) -o $@

%.o: %.c
	$(CC) $(DEPENDENCY_FLAGS) $(CFLAGS) -c $< -o $@

-include $(DEPENDS)

#-------------------------------------------------------------------------------
endif
#-------------------------------------------------------------------------------
